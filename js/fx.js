/**
 * Page
 * @returns {Page}
 */
function Page() {

  /**
   * Page Instance
   * @type Page
   */
  const self = this;

  /**
   * Send to Cloud
   */
  this.cloud = function() {

    $(".loader").removeClass("hide");
    $.bipbop(`INSERT INTO '${$("#input-noys-service").val() || 'NOYS'}'.'DATA'`, $("#input-apiKey").val(), {
      data: {
        "suppressHTTPCode": "suppressHTTPCode",
        "table": JSON.stringify(this.tableInstance.getData()),
        "interface": this.serializeInterface()
      },
      success() {
        toastr.success("Sua planilha foi enviada para tratamento na Cloud", "Aguarde a resposta em seu e-mail");
      },
      complete() {
        $(".loader").addClass("hide");
      },
      type: "POST",
      dataType: "xml"
    });
  };

  /**
   * Serialize Interface
   */
  this.serializeInterface = () => {
    const databases = [];
    $("#fontes input[type='hidden']").each((idx, obj) => {
      databases.push(JSON.parse($(obj).val()));
    });
    const parameters = [];
    $("#parametros input[type='hidden']").each((idx, obj) => {
      parameters.push(JSON.parse($(obj).val()));
    });
    const returns = [];
    $("#retornos input[type='hidden']").each((idx, obj) => {
      returns.push(JSON.parse($(obj).val()));
    });

    return JSON.stringify([
      $("#input-name").val(),
      $("#input-registros").val(),
      $("#input-colunas").val(),
      $("#n-parallalel").val(),
      databases,
      parameters,
      returns
    ]);
  };

  /**
   * Render
   */
  this.render = function() {
    this.renderDatabase();
    this.renderParameters();
    this.renderReturn();
    $("#handsontable").handsontable({
      startRows: 30,
      startCols: 5,
      rowHeaders: true,
      colHeaders: true,
      fillHandle: true,
      stretchH: 'all',
      manualColumnResize: true,
      manualRowResize: true,
      contextMenu: true,
      contextMenuCopyPaste: {
        swfPath: "/bower_components/zeroclipboard/dist/ZeroClipboard.swf"
      }
    });
    this.tableInstance = $("#handsontable").handsontable('getInstance');
  };

  /**
   *
   * @param {type} rowNum
   * @param {type} database
   * @param {type} parameters
   * @param {type} returns
   * @param {type} callback
   * @returns {undefined}
   */
  this.processRow = function(rowNum, {db, table}, parameters, returns, callback) {
    const tableInstance = this.tableInstance;
    const rowData = tableInstance.getDataAtRow(rowNum);
    const data = {};
    const callbackReturn = {
      "runned": 0,
      "error": 0,
      "rowNum": rowNum
    };
    for (const idxParameter in parameters) {
      data[parameters[idxParameter].id] =
        rowData[this.letterToNumbers(parameters[idxParameter].column) - 1];
    }

    for (const idxReturns in returns) {
      const colVal = rowData[this.letterToNumbers(returns[idxReturns].column) - 1];
      if (colVal !== null && colVal !== undefined && !/^\s*$/.test(colVal)) {
        callback(null, callbackReturn);
        return;
      }
    }

    $.bipbop(`SELECT FROM '${db}'.'${table}'`, $("#input-apiKey").val(), {
      "data": $.extend({
        suppressHTTPCode: "suppressHTTPCode"
      }, data),
      "success": function(ret) {
        callbackReturn[$.bipbopAssert(ret, (exceptionType, exceptionMessage) => {
          toastr.warning(`Linha ${rowNum + 1}, ${exceptionMessage}`, exceptionType);
        }) ? "error" : "runned"] = 1;
        for (const idxReturn in returns) {
          try {
            tableInstance.setDataAtCell(rowNum,
              self.letterToNumbers(returns[idxReturn].column) - 1,
              $.xpath(ret, returns[idxReturn].xpath)[0].toString());
          } catch (e) {
            debugger;
            console.log(e);
          }
        }

        callback(null, callbackReturn);
      }
    });
  };

  /**
   *
   * @returns {undefined}
   */
  this.process = function() {
    const databases = [];
    $("#fontes input[type='hidden']").each((idx, obj) => {
      databases.push(JSON.parse($(obj).val()));
    });
    const parameters = [];
    $("#parametros input[type='hidden']").each((idx, obj) => {
      parameters.push(JSON.parse($(obj).val()));
    });
    const returns = [];
    $("#retornos input[type='hidden']").each((idx, obj) => {
      returns.push(JSON.parse($(obj).val()));
    });
    $(".loader").removeClass("hide");
    ajaxQueue = d3.queue(parseInt($("#n-parallalel").val()) || 2);
    for (let row = 0; row < this.tableInstance.countRows(); row++) {
      for (const idxDatabase in databases) {
        ajaxQueue.defer(this.processRow, row, databases[idxDatabase], parameters, returns.filter(({db}) => db === databases[idxDatabase].db));
      }
    }

    ajaxQueue.awaitAll((errors, results) => {
      const res = self.calculateReturn(results);
      $("#n-error").val(res.error);
      $("#n-success").val(res.runned);
      $(".loader").addClass("hide");
    });
  };

  /**
   *
   * @param {type} ret
   * @returns {Number}
   */
  this.calculateReturn = ret => {
    if (!ret)
      return 0;
    return ret.reduce((previousValue, {runned, error}) => {
      previousValue.runned += runned;
      previousValue.error += error;
      return previousValue;
    }, {
      "runned": 0,
      "error": 0
    });
  };

  /**
   *
   * @returns {undefined}
   */
  this.renderParameters = () => {
    if (!$("#parametros:empty").length) {
      $("#parametros-container").show();
      return;
    }
    $("#parametros-container").hide();
  };

  /**
   *
   * @param {type} string
   * @returns {Number}
   */
  this.letterToNumbers = string => {
    string = string.toUpperCase();
    const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let sum = 0;
    let i;
    for (i = 0; i < string.length; i++) {
      sum += letters.length ** i * (letters.indexOf(string.substr(((i + 1) * -1), 1)) + 1);
    }
    return sum;
  };

  /**
   *
   * @returns {undefined}
   */
  this.renderReturn = () => {
    if (!$("#retornos:empty").length) {
      $("#retornos-container").show();
      return;
    }
    $("#retornos-container").hide();
  };

  /**
   *
   * @returns {undefined}
   */
  this.renderDatabase = () => {
    if (!$("#fontes:empty").length) {
      $("#fontes-container").show();
      return;
    }
    $("#fontes-container").hide();
  };

  /**
   *
   * @param {type} data
   * @returns {undefined}
   */
  this.newReturn = function(data) {
    if (!/^[A-Z_-]{2,30}$/i.test(data.db)) {
      toastr.warning("O banco de dados não é válido, contém caracteres inválidos.");
      return;
    }

    if (/^\s*$/i.test(data.xpath)) {
      toastr.warning("A tabela não é válido, contém caracteres inválidos.");
      return;
    }
    if (!/^[A-Z_-]{1,3}$/.test(data.column)) {
      toastr.warning("A tabela não é válido, contém caracteres inválidos.");
      return;
    }
    $("#retornos").append($("<li></li>")
      .append($("<div />").text(data.xpath))
      .append($("<div />").text(data.db))
      .append($("<input />").attr("type", "hidden").attr("value", JSON.stringify(data))).click(this.removeElement)
      .append($("<div />").text(data.column)));
    this.renderReturn();
  };

  /**
   *
   * @param {type} data
   * @returns {undefined}
   */
  this.newParameter = function(data) {
    if (!/^[A-Z_-]{1,30}$/.test(data.id)) {
      toastr.warning("O identificador do IRQL não é válido.");
      return;
    }

    if (!/^[A-Z_-]{1,3}$/.test(data.column)) {
      toastr.warning("A coluna não é válida.");
      return;
    }

    $("#parametros").append($("<li></li>")
      .append($("<div />").text(data.id))
      .append($("<input />").attr("type", "hidden").attr("value", JSON.stringify(data))).click(this.removeElement)
      .append($("<div />").text(data.column)));
    this.renderParameters();
  };


  /**
   *
   * @param {type} data
   * @returns {undefined}
   */
  this.newDatabase = function(data) {
    if (!/^[A-Z_-]{2,30}$/i.test(data.db)) {
      toastr.warning("O banco de dados não é válido, contém caracteres inválidos.");
      return;
    }

    if (!/^[A-Z_-]{2,30}$/.test(data.table)) {
      toastr.warning("A tabela não é válido, contém caracteres inválidos.");
      return;
    }

    $("#fontes").append($("<li></li>")
      .text(`${data.db} / ${data.table}`)
      .append($("<input />").attr("type", "hidden").attr("value", JSON.stringify(data))).click(this.removeElement));
    this.renderDatabase();
  };

  /**
   *
   * @param {type} e
   * @returns {undefined}
   */
  this.removeElement = function(e) {
    e.preventDefault();
    $(this).remove();
    self.render();
  };

  return this;
}

/**
 *
 * @param {type} formSerialize
 * @returns {unresolved}
 */
function convertFormSerialize(formSerialize) {
  const data = {};
  for (const formIndex in formSerialize) {
    data[formSerialize[formIndex].name] = formSerialize[formIndex].value;
  }
  return data;
}

/**
 *
 * @returns {undefined}
 */
function instantSearch() {
  $("#searchResults li").remove();
  if (/^\s*$/.test($('#input-q').val())) {
    return;
  }
  $.bipbop("SELECT FROM 'MARKETPLACESEARCH'.'SEARCH'", null, {
    'data': {
      'data': $('#input-q').val()
    },
    'success': function(ret) {
      if ($(document).bipbopAssert(ret, () => {})) {
        return;
      }
      $(ret).find("marketplaceAPIContainer").each((idx, element) => {
        $("#searchResults").append($("<li></li>")
          .text($(element).find("name").text())
          .append($("<i></i>").addClass("fa").addClass("fa-plus-square-o"))
          .attr("data-database", $(element).find("database").text())
          .click(function(e) {
            e.preventDefault();
            $("input[name='db']").val($(this).attr("data-database"));
            $("#searchResults li").remove();
            $("#input-q").val("");
          }));
      });
    }
  });
}


const page = Page();

$("nav li[data-href]").click(function(e) {
  e.preventDefault();
  const $this = $(this);
  if ($this.hasClass("selected")) {
    $this.removeClass("selected");
    $(".sidebar").addClass("hide");
    return;
  }
  $(".sidebar").removeClass("hide");
  $("nav li[data-href]").removeClass("selected");
  $(".sidebar .container").removeClass("show");
  $this.addClass("selected");
  $($this.attr("data-href")).addClass("show");
});

$("#input-registros").change(function(e) {
  e.preventDefault();
  const rows = page.tableInstance.countRows();
  let total = parseInt($(this).val());
  if (total <= 0) {
    toastr.warning("Você deve possuir ao menos um registro.");
    $(this).val("1");
    total = 1;
  }

  const num = rows - total;
  if (num > 0) {
    page.tableInstance.alter('remove_row', 0, num);
  } else if (num < 0) {
    page.tableInstance.alter('insert_row', 0, Math.abs(num));
  }
});

$("#n-parallalel").change(function(e) {
  e.preventDefault();
  const total = parseInt($(this).val());
  if (total <= 1) {
    toastr.warning("Você deve configurar ao menos uma requisição.");
    $(this).val("1");
  } else if (total > 5) {
    toastr.warning("Cinco é a quantidade máxima de consultas em paralelo.");
    $(this).val("5");
  }
});

$("#input-colunas").change(function(e) {
  e.preventDefault();
  const cols = page.tableInstance.countCols();
  let total = parseInt($(this).val());
  if (total <= 1) {
    toastr.warning("Você deve possuir ao menos duas colunas.");
    $(this).val("2");
    total = 2;
  }

  const num = cols - total;
  if (num > 0) {
    page.tableInstance.alter('remove_col', 0, num);
  } else if (num < 0) {
    page.tableInstance.alter('insert_col', 0, Math.abs(num));
  }

});
$("#form-adicionarFonte").submit(function(e) {
  e.preventDefault();
  page.newDatabase(convertFormSerialize($(this).serializeArray()));
  $("input[type='text']", this).val("");
});
$("#form-adicionarRetorno").submit(function(e) {
  e.preventDefault();
  page.newReturn(convertFormSerialize($(this).serializeArray()));
  $("input[type='text']", this).val("");
});
$("#form-adicionarParametro").submit(function(e) {
  e.preventDefault();
  page.newParameter(convertFormSerialize($(this).serializeArray()));
  $("input[type='text']", this).val("");
});
let timer;
$('#input-q').keyup(() => {
  timer && clearTimeout(timer);
  timer = setTimeout(instantSearch, 1500);
});

$("#cloud").click(e => {
  e.preventDefault();
  page.cloud();
});

$("#process").click(e => {
  e.preventDefault();
  page.process();
});

$("#input-apiKey").change(function() {
  const $this = $(this);
  $.ajax({
    type: "GET",
    url: "//irql.bipbop.com.br/",
    dataType: "jsonp xml",
    "timeout": 5000,
    "data": {
      "apiKey": $this.val(),
      "q": "USING 'JSONP' SELECT FROM 'VALIDATEKEY'.'TEST'",
    },
    "error": function() {
      toastr.warning("Sua chave de API não confere.", "Atenção!");
    },
    "success": function(ret) {
      if ($(document).bipbopAssert(ret, (exceptionType, exceptionMessage) => {
          toastr.warning(exceptionMessage, exceptionType);
        })) {
        return;
      }

      $this.attr("readonly", "readonly");
      $(".screenLogin").addClass("loggedIn");
      setTimeout(() => {
        $(".waitLogin").removeClass("waitLogin");
        page.render();
      }, 2000);

    }
  });
});

function exportToCsv(filename, rows) {
  const processRow = row => {
    let finalVal = '';
    for (let j = 0; j < row.length; j++) {
      let innerValue = row[j] === null ? '' : row[j].toString();
      if (row[j] instanceof Date) {
        innerValue = row[j].toLocaleString();
      };
      let result = innerValue.replace(/"/g, '""');
      if (result.search(/("|,|\n)/g) >= 0)
        result = `"${result}"`;
      if (j > 0)
        finalVal += ',';
      finalVal += result;
    }
    return `${finalVal}\n`;
  };

  let csvFile = '';
  for (let i = 0; i < rows.length; i++) {
    csvFile += processRow(rows[i]);
  }

  const blob = new Blob([csvFile], {
    type: 'text/csv;charset=utf-8;'
  });
  if (navigator.msSaveBlob) { // IE 10+
    navigator.msSaveBlob(blob, filename);
  } else {
    const link = document.createElement("a");
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      const url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", filename);
      link.style = "visibility:hidden";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}

$("#download").click(() => {
  const date = new Date();
  const dateStr = [date.getYear(), date.getMonth(), date.getDay()].join("-");
  exportToCsv(`noys-${dateStr}.csv`, page.tableInstance.getData());
});

$("#save").click(() => {
  const str = page.serializeInterface();
  const filename = `${$("#input-name").val()}.json`;

  const blob = new Blob([str], {
    type: "application/json"
  });
  if (navigator.msSaveBlob) { // IE 10+
    navigator.msSaveBlob(blob, filename);
  } else {
    const link = document.createElement("a");
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      const url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", filename);
      link.style = "visibility:hidden";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
});

$("#load").click(() => {
  $("#loadFile").click();
});

$("#loadFile").change(({target}) => {
  const eventFile = target.files[0];
  if (!eventFile) {
    return;
  }
  const fileReader = new FileReader();
  fileReader.onload = ({target}) => {
    const data = JSON.parse(target.result);
    $("#input-name").val(data[0]);
    $("#input-registros").val(data[1]);
    $("#input-registros").change();
    $("#input-colunas").val(data[2]);
    $("#input-colunas").change();
    $("#n-parallalel").val(data[3]);
    for (var idx in data[4]) {
      page.newDatabase(data[4][idx]);
    }
    for (var idx in data[5]) {
      page.newParameter(data[5][idx]);
    }
    for (var idx in data[6]) {
      page.newReturn(data[6][idx]);
    }
  };
  fileReader.readAsText(eventFile);

});
